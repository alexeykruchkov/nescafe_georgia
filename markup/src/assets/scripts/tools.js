$(document).ready(function(){
    
  //tabs switch
  $('ul.tabs li').on("click", function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('active');
    $('.tab-content').removeClass('active');

    $(this).addClass('active');
    $("#"+tab_id).addClass('active');
  });

  //popup trigger
  $('.gallery-list__pic-wr').on("click", function(e){
    e.preventDefault;
    $('body').addClass("overflow");
    $('.popup-wr').addClass('active');
    $(".popup").fadeIn(300,function(){$(this).focus();});
  });

  $('.popup .closer').on("click", function(){
    $('body').removeClass("overflow");
    $('.popup-wr').removeClass('active');
    $(".popup").fadeOut(300);
  });
  $(".popup").on("blur keydown", function(){
    $(this).fadeOut(300);
    $('body').removeClass("overflow");
    $('.popup-wr').removeClass('active');
  });

  //map popup close
  $('.map__popup .closer').on("click", function(){
    $(this).parent('.map__popup').hide();
  });

  //gallery expand arrows rotation
  $('.gallery-list__expand-more').on("click", function(){
    $('.gallery-list__expand-more span').addClass('rotate');
  });

  //textarea left symbols counter
  $("#textarea").keyup(function(){
    $("#count").show();
    $("#count span").text((200 - $(this).val().length));
  });

  //stickers carousel custom scrollbar
  $(".carousel").mCustomScrollbar({
    axis:"x",
    theme:"light-3",
    advanced:{autoExpandHorizontalScroll:true}
  });
});